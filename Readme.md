# Some Metrics

A service to sum metrics. Based on express-generator boilerplate and GitHub Node.gitignore.

Based on the spec not mentioning input validation, testing, error states, etc, I'm assuming that this is a test of my ability to do basic coding (akin to 'fizzbuzz') and not an example of a production service.

## Install dependencies:

`$ npm install`

## Run the app:

`$ DEBUG=some-metrics:* npm start`

## API

### GET /metric/{key}/sum

Returns the sum of all metrics reported for this key over the past hour (Rolling 60 minute window).

Example call (using [httpie](https://httpie.org/)): `http http://localhost:3000/metric/eric/sum`

### POST /metric/{key}

Submit metric for a key.

Example call (using [httpie](https://httpie.org/)): `http POST http://localhost:3000/metric/eric value:=10`
