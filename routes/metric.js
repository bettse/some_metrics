const express = require('express');
const router = express.Router();

const oneHour = 1000 * 60 * 60;
const DECIMAL = 10;

// {
//   'key': [
//     { ts: unix_timestamp, value: value }
//   ]
// }
const DB = {};

router.get('/:key/sum', function(req, res, next) {
  const {params} = req;
  const {key} = params;

  const metrics = DB[key];
  if (!metrics || metrics.length === 0) {
    res.status(200).json({value: 0});
    return;
  }

  const value = Math.round(metrics.reduce((sum, {value}) => sum + value, 0));
  res.status(200).json({value});
});

router.post('/:key', function(req, res, next) {
  const now = Date.now();
  const {body, params} = req;
  const {value} = body;
  const {key} = params;

  const metrics = DB[key] || [];
  // Only keep recent metrics
  const lastHour = metrics.filter(metric => {
    const {ts} = metric;
    return now - ts < oneHour;
  });
  const saneValue = Math.round(parseInt(value, DECIMAL) || 0);
  lastHour.push({ts: now, value: saneValue});
  DB[key] = lastHour;

  res.status(200).json({});
});

module.exports = router;
